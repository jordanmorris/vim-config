set nocompatible
set nobackup

"clipboard stuff BEGIN
noremap L :t.<CR>
vnoremap <C-C> "+y
noremap X "_X
noremap x "_x
set clipboard=unnamed,unnamedplus
"clipboard stuff END

" Remove dos linebreak junk
:command Noms :%s/\r$//

" Format json
:command Fj set syntax=json | :%!jq .

autocmd FileType markdown :setlocal spell

colorscheme darkblue
set wrap
set linebreak
set nolist  " list disables linebreakset textwidth=0
set wrapmargin=0
set shiftwidth=4
set tabstop=4
set expandtab
set number

map <A-p> :CtrlPBuffer<CR>

filetype plugin on
set omnifunc=syntaxcomplete#Complete


"NERDTree stuff BEGIN
"How can I close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

map <C-n>n :NERDTreeToggle<CR>
"map <C-n>1 :NERDTreeFromBookmark Club3<CR>

"set work directory to tree root when root changed
let g:NERDTreeChDirMode=2

"NERDTree stuff END

